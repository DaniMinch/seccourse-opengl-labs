#-------------------------------------------------
#
# Project created by QtCreator 2016-03-09T18:10:41
#
#-------------------------------------------------

QT       += opengl

CONFIG	 += C++14

QMAKE_LFLAGS_RELEASE += -static -static-libgcc

TARGET = OpenGlTest1
TEMPLATE = app


SOURCES += main.cpp\
    imageloader.cpp \
    vec3f.cpp

HEADERS  += imageloader.h \
    vec3f.h

LIBS += libfreeglut -lopengl32 -lglu32
