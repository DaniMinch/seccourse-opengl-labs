#define _USE_MATH_DEFINES

#include <GL/gl.h>
//#include <GL/glu.h>
#include <GL/freeglut.h>
#include <imageloader.h> // загрузчик текстур
#include <math.h> // синусы и float-вычисления
#include <vec3f.h> // вектор 3 флоатов
#include <vector>


GLuint _textureId; // текстура и квадрик для лаб 1 и 2
GLUquadricObj* cone;
/*Lab1
 * ------------------------------------------------
 */
GLfloat xangle1 = 0; // крутилка
GLfloat yangle1 = 0;
GLfloat zangle1 = 0;


void display_lab1 () {

    // clear window
    glClearColor(1, 1, 1, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glColor3d(0, 0, 0);
    // draw scene
    glPushMatrix();

    glRotatef (xangle1, 1.f, 0.f, 0.f);  // Up and down arrow keys 'tip' view.
    glRotatef (yangle1, 0.f, 1.f, 0.f);  // Right/left arrow keys 'turn' view.
    glRotatef (zangle1, 0.f, 0.f, 1.f );  // pgup/pgdwn keys

    // teapot inside cube
    glPushMatrix();
    glTranslated(-.5, .5, 0); // перемещение центра координат
    glRotated(60, 1, 1, 0); // поврот вокруг соотв осей
    glutWireTeapot(.20);
    glRotated(60, 1, 1, 0);
    glColor3d(0, 0, 1);
    glutWireCube(.6);
    glPopMatrix();

    // sphere in the apex of cone
    gluQuadricDrawStyle(cone, GLU_FILL); // понадобится для текстурирования
    glPushMatrix();
    glTranslated(.5, .5, 0);
    glRotated(100, -1, 0, 0);
    glRotated(60, 0, 1, 0);
    gluCylinder(cone, .2, 0, .3, 32, 32); // конус

    glPushMatrix();
    glRotated(180, 1, 0, 0);
    gluDisk(cone, 0, .2, 32, 4); // основание
    glPopMatrix();

    glTranslated(0, 0, .3); // подвинули на высоту конуса
    glColor3d(0.34, 0.82, 0.72);
    gluSphere(cone, .1, 32, 32);
    glPopMatrix();

    // sphere in the center of cone base
    glPushMatrix();
    glColor3d(0.43, 0.72, 0.10);
    glTranslated(-.5, -.5, 0);
    glRotated(80, 1, 0, 0);
    gluCylinder(cone, .2, 0, .3, 32, 32);
    glPushMatrix();
    glRotated(180, 1, 0, 0);
    gluDisk(cone, 0 , .2, 32, 4);
    glPopMatrix();
    glColor3d(1, 0, 1);
    gluSphere(cone, .1, 32, 32);
    glPopMatrix();

    glColor3d(1, 1, 1);
    // same textured thing
    glEnable(GL_TEXTURE_2D); // включаем тестурирование
    // что-то очень важное
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glPushMatrix();
    glTranslated(.5, -.5, 0);
    glRotated(280, 1, 0, 0);
    glRotated(40, 0, 1, 0);
    glRotated(90, 0, 0, 1);
    gluSphere(cone, .1, 32, 32);
    gluCylinder(cone, .2, 0, .3, 32, 32);
    glPushMatrix();
    glRotated(180, 1, 0, 0);
    gluDisk(cone, 0, .2, 32, 32);
    glPopMatrix();
    glPopMatrix();
    gluDeleteQuadric(cone);

    glDisable(GL_TEXTURE_2D);

    glPopMatrix();


    // flush drawing routines to the window
    glFlush();

}

/* EOLab1
 * ------------------------------------------------
 * Lab 2
 */

GLfloat xangle2 = 0;
GLfloat yangle2 = 0;
GLfloat zangle2 = 0;

// дефолтные положения камер
// w=1 - свет из указанной координаты вокруг себя
// w= 0 - свет в 0,0,0 из беск. по напр. точки
// w = 1 SPOT параметры - прожектор
float light0_position[] = {0.0f, 0.5f, .0f, 1.0f};
float light1_position[] = {0.0f, 10.0f, 0.0f, 1.0f};
float light2_position[] = {0.0f, 0.0f, 3.0f, 0.0f};

// здесь прописываются параметры света
void createLights()
{

    glEnable(GL_LIGHTING); // включаем освещение
    // точечный
    float light0_diffuse[] = {1.0f, 0.0f, 0.0f, 1.0f}; // цвет
    float light0_ambient[] = {0.0f, 0.0f, 0.0f, 0.0f}; // ???
    float light0_specular[] = {1.0f, 0.0f, 0.0f, 1.0f}; // цвет отражаемого

    glEnable(GL_LIGHT0); // разрешаем использовать light0

    glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_diffuse);
    glLightfv(GL_LIGHT0, GL_AMBIENT, light0_ambient);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light0_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light0_position);


    float light1_diffuse[] = {1.0f, 1.0f, .0f, 1.0f};
    float light1_ambient[] = {0.0f, 0.0f, 0.0f, 0.0f};
    float light1_specular[] = {0.0f, 0.0f, 1.0f, 1.0f};

    glEnable(GL_LIGHT1); // разрешаем использовать light1

    glLightfv(GL_LIGHT1, GL_DIFFUSE, light1_diffuse);
    glLightfv(GL_LIGHT1, GL_AMBIENT, light1_ambient);
    glLightfv(GL_LIGHT1, GL_SPECULAR, light1_specular);
    glLightfv(GL_LIGHT1, GL_POSITION, light1_position);

    float light2_diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
    float light2_ambient[] = {0.0f, 0.0f, 0.0f, 0.0f};
    float light2_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
    //float light2_spot_direction[] = {-1.0f, 0.0f, 0.0f, 1.0f};
    //float light2_spot_cutoff[] = {40.f};
    //float light2_spot_exp[] = {1.f};

    glEnable(GL_LIGHT2); // разрешаем использовать light2

    glLightfv(GL_LIGHT2, GL_DIFFUSE, light2_diffuse);
    glLightfv(GL_LIGHT2, GL_AMBIENT, light2_ambient);
    glLightfv(GL_LIGHT2, GL_SPECULAR, light2_specular);
    glLightfv(GL_LIGHT2, GL_POSITION, light2_position);
    //glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light2_spot_direction);
    //glLightfv(GL_LIGHT2, GL_SPOT_CUTOFF, light2_spot_cutoff);
    //glLightfv(GL_LIGHT2, GL_SPOT_EXPONENT, light2_spot_exp);

}

// перемещение источников в соответствии с координатами по кнопкам
void updateLightsPosition()
{
//    qDebug()<<'0'<<light0_position[0]<<light0_position[1]<<light0_position[2]<<light0_position[3];
//    qDebug()<<'1'<<light1_position[0]<<light1_position[1]<<light1_position[2]<<light1_position[3];
//    qDebug()<<'2'<<light2_position[0]<<light2_position[1]<<light2_position[2]<<light2_position[3];
    glLightfv(GL_LIGHT0, GL_POSITION,light0_position);
    glLightfv(GL_LIGHT1, GL_POSITION,light1_position);
    glLightfv(GL_LIGHT2, GL_POSITION,light2_position);
}

void display_lab2()
{

    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glPushMatrix();
    glRotatef (xangle2, 1.f, 0.f, 0.f);  // Up and down arrow keys 'tip' view.
    glRotatef (yangle2, 0.f, 1.f, 0.f);  // Right/left arrow keys 'turn' view.
    glRotatef (zangle2, 0.f, 0.f, 1.f );  // pgup/pgdwn keys

    updateLightsPosition();

    glColor3d(1,0,0);

    glEnable(GL_TEXTURE_2D);
    // еще больше очень важных настроек
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    // описываем свойства материала (бронза)
    float diffuse[4] = {.714f, .4284f, .18144f, 1.f};
    float ambient[4] = {0.f, 0.f, 0.f, 1.f};
    float specular[4] = {.393548f, .271906f, .166721f, 1.f};
    float shininess [1] = { 25.6f };
    glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse); // цвет чайника
    glMaterialfv(GL_FRONT, GL_SPECULAR, specular); // отраженный свет
    glMaterialfv(GL_FRONT, GL_SHININESS, shininess); // степень отраженного света
    //----------------------------
    glPushMatrix();
    glTranslated(-1, .66, 0);
    // рисуем чайник с помощью библиотеки FreeGLUT
    glScaled(.5,.5,.5);
    glutSolidTeapot(.5);
    glPopMatrix();

    float diffuse_cube[4] = {0.0f, 1.0f, 0.f, 1.f};
    float ambient_cube[4] = {0.f, 0.f, 0.f, 1.f};
    float specular_cube[4] = {.4f, .8f, 0.f, 1.f};
    float shininess_cube [1] = { 9.f };

    glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse_cube); // цвет кубика
    glMaterialfv(GL_FRONT, GL_AMBIENT, ambient_cube);
    glMaterialfv(GL_FRONT, GL_SPECULAR, specular_cube); // отраженный свет
    glMaterialfv(GL_FRONT, GL_SHININESS, shininess_cube); // степень отраженного света
    //----------------------------
    glPushMatrix();
    glTranslated(-1,0,0);
    float size = 1.f;
    glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
    glTexGenfv(GL_S, GL_OBJECT_PLANE, new float[4]{(float)(1.f / size), 0.f, 0.f, .5f});

    glTexGenf(GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
    glTexGenfv(GL_T, GL_OBJECT_PLANE, new float[4]{0.f, (float)(-1.f / size), 0.f, .5f});

    glEnable(GL_TEXTURE_GEN_S);
    glEnable(GL_TEXTURE_GEN_T);
    glutSolidCube(size);
    glDisable(GL_TEXTURE_GEN_S);
    glDisable(GL_TEXTURE_GEN_T);
    glPopMatrix();

    gluQuadricDrawStyle(cone, GLU_FILL); // квадрик заполнен, с текстурой и сглаженными нормалями
    gluQuadricTexture(cone, TRUE);
    gluQuadricNormals(cone, GLU_SMOOTH);

    float diffuse_sphere[4] = {1.0f, 1.0f, 1.0f, .5f};
    float ambient_sphere[4] = {0.2f, 0.2f, 0.2f, .5f};
    float specular_sphere[4] = {1.f, 1.f, 1.f, .5f};
    float shininess_sphere [1] = { 1.f };
    glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse_sphere); // цвет шарика
    glMaterialfv(GL_FRONT, GL_AMBIENT, ambient_sphere);
    glMaterialfv(GL_FRONT, GL_SPECULAR, specular_sphere); // отраженный свет
    glMaterialfv(GL_FRONT, GL_SHININESS, shininess_sphere); // степень отраженного света
    //----------------------------
    glPushMatrix();
    glTranslated(2, 1, -1);
    gluSphere(cone, 1.0f, 32, 32); // координаты для текстуры генерятся автоматически
    glPopMatrix();

    glPopMatrix();

    glFlush();
}

// обработчик клавиш для пермеения и выключения камер
void keyHandlerLab2(unsigned char key, int xmouse, int ymouse)
{
    switch (key) {
    // перемещение точечного красного света
    case 'a':
        light0_position[0] -= .5f;
        break;

    case 'd':
        light0_position[0] += .5f;
        break;

    case 'w':
        light0_position[1] += .5f;
        break;

    case 's':
        light0_position[1] -= 0.5f;
        break;

    case 'q':
        light0_position[2] -= .5f;
        break;

    case 'e':
        light0_position[2] += .5f;
        break;

        // перемещение точечного синего цвета
    case 'f':
        light1_position[0] -= .5f;
        break;

    case 'h':
        light1_position[0] += .5f;
        break;

    case 't':
        light1_position[1] += .5f;
        break;

    case 'g':
        light1_position[1] -= .5f;
        break;

    case 'r':
        light1_position[2] -= .5f;
        break;

    case 'y':
        light0_position[2] += .5f;
        break;

        // перемещение направленного света
    case 'j':
        light2_position[0] -= .5f;
        break;

    case 'l':
        light2_position[0] += .5f;
        break;

    case 'i':
        light2_position[1] += .5f;
        break;

    case 'k':
        light2_position[1] -= .5f;
        break;

    case 'u':
        light2_position[2] -= .5f;
        break;

    case 'o':
        light2_position[2] += .5f;
        break;
    case 'z':
        glEnable(GL_LIGHT0);
        break;
    case 'x':
        glDisable(GL_LIGHT0);
        break;
    case 'c':
        glEnable(GL_LIGHT1);
        break;
    case 'v':
        glDisable(GL_LIGHT1);
        break;
    case 'b':
        glEnable(GL_LIGHT2);
        break;
    case 'n':
        glDisable(GL_LIGHT2);
        break;
    default:
        return;
    }
    glutPostRedisplay();
}

/* EOLab2
 * ------------------------------------------------
 * Lab 1_Morphing
 */

GLfloat xangle3 = 0;
GLfloat yangle3 = 0;
GLfloat zangle3 = 0;

bool isSolid = false;
int stacks = 128, slices = 128; // количество широт и долгот шарика и конуса
float radius = 1.f; // радиус
float morphStage = 0.f; // начальное состояния морфинга - шар

std::vector<Vec3f>* pointsSphere; // точки шарика
std::vector<Vec3f>* pointsCone; // конуса
std::vector<Vec3f>* pointsMorph; // морфинга

// непосредственно преобразования
void morph()
{
    for(auto i = 0; i < pointsSphere->size(); i++) {
        Vec3f p1 = pointsSphere->at(i);
        Vec3f p2 = pointsCone->at(i);
        Vec3f direction(p1);
        direction -= p2; // общая разница между начальным и конечным
        direction *= morphStage; // процент завершенности преобразования
        direction += p2; // возвращаем в реальные координаты
        pointsMorph->at(i) = direction; // обновляем данные
    }
}

// dispplay для морфинга
void morphing()
{
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    morph(); // сначала пересчитываем координаты

    glPushMatrix();
    glRotatef (xangle3, 1.f, 0.f, 0.f);  // Up and down arrow keys 'tip' view.
    glRotatef (yangle3, 0.f, 1.f, 0.f);  // Right/left arrow keys 'turn' view.
    glRotatef (zangle3, 0.f, 0.f, 1.f );  // pgup/pgdwn keys

    glPushMatrix();
    //glScaled(4,4,4);
    // цвет динамический
    glColor3f(morphStage, 1.f - morphStage, morphStage);
    // увеличиваем размер каждой точки
    glPointSize(2.f);
    // рисуем фигуру по точкам
    if (isSolid)
    {
        // треугольники с общей вершиной для верха
        // кф i получены при генерации
        glBegin(GL_TRIANGLE_FAN);
        {
            for (auto i = 0; i < slices + 2; i++)
            {
                glVertex3f(pointsMorph->at(i)[0], pointsMorph->at(i)[1],
                        pointsMorph->at(i)[2]);
            }
        }
        glEnd();
        // связанные квадраты для боковой поверхности
        glBegin(GL_QUAD_STRIP);
        {
            for (auto i = slices + 2; i < (((stacks - 2)* (slices+1) * 2) + slices + 2); i++)
            {
                glVertex3f(pointsMorph->at(i)[0], pointsMorph->at(i)[1],
                        pointsMorph->at(i)[2]);
            }
        }
        glEnd();
        // треугольники с общей вершиной для низа
        glBegin(GL_TRIANGLE_FAN);
        {
            for (auto i = (pointsMorph->size()- slices - 3);
                 i < pointsMorph->size(); i++)
            {
                glVertex3f(pointsMorph->at(i)[0], pointsMorph->at(i)[1],
                        pointsMorph->at(i)[2]);
            }
        }
        glEnd();
    }
    else
    {
        // просто точки
        glBegin(GL_POINTS);
        {
            for (auto it = pointsMorph->begin(); it < pointsMorph->end(); it++)
            {
                glVertex3f((*it)[0], (*it)[1], (*it)[2]);
            }
        }
        glEnd();
    }
    glPopMatrix();

    glPopMatrix();
    glFlush();
}

// генерация точек сферы - украдено из исходников
void generateSpherePoints()
{
    pointsSphere = new std::vector<Vec3f>;

    float x, y, z;
    float rho, drho, theta, dtheta;
    float s, t, ds, dt;
    int i, j, imin, imax;
    drho = (float)M_PI / stacks;
    dtheta = (float)M_PI * 2.f/ slices;

    if(isSolid)
    {
        // draw +Z end as a triangle fan
        //glBegin(GL_TRIANGLE_FAN);
        pointsSphere->push_back(Vec3f(0.0f, 0.0f, radius));
        for (j = 0; j <= slices; j++) {
            theta = (j == slices) ? 0.0f : j * dtheta;
            x = -sin(theta) * sin(drho);
            y = cos(theta) * sin(drho);
            z = cos(drho);
            pointsSphere->push_back(Vec3f(x * radius, y * radius, z * radius));
        }
        //glEnd();


        ds = 1.0f / slices;
        dt = 1.0f / stacks;
        t = 1.0f; // because loop now runs from 0
        imin = 1;
        imax = stacks - 1;

        // draw intermediate stacks as quad strips
        for (i = imin; i < imax; i++) {
            rho = i * drho;
            //glBegin(GL_QUAD_STRIP);
            s = 0.0f;
            for (j = 0; j <= slices; j++) {
                theta = (j == slices) ? 0.0f : j * dtheta;
                x = -sin(theta) * sin(rho);
                y = cos(theta) * sin(rho);
                z = cos(rho);
                pointsSphere->push_back(Vec3f(x * radius, y * radius, z * radius));
                x = -sin(theta) * sin(rho + drho);
                y = cos(theta) * sin(rho + drho);
                z = cos(rho + drho);
                s += ds;
                pointsSphere->push_back(Vec3f(x * radius, y * radius, z * radius));
            }
            //glEnd();
            t -= dt;
        }
        // draw -Z end as a triangle fan
        //glBegin(GL_TRIANGLE_FAN);
        pointsSphere->push_back(Vec3f(0.0f, 0.0f, -radius));
        rho = M_PI - drho;
        s = 1.0f;
        for (j = slices; j >= 0; j--) {
            theta = (j == slices) ? 0.0f : j * dtheta;
            x = -sin(theta) * sin(rho);
            y = cos(theta) * sin(rho);
            z = cos(rho);
            pointsSphere->push_back(Vec3f(x * radius, y * radius, z * radius));
        }
        //glEnd();
    }
    else
    {
        pointsSphere->push_back(Vec3f(0.f, 0.f, radius));

        for (int i = 1; i <= stacks - 1; i++) {
            rho = i * drho;
            for (int j = 0; j < slices; j++) {
                theta = j * dtheta;
                x = (float)(cos(theta) * sin(rho));
                y = (float)(sin(theta) * sin(rho));
                z = (float)(cos(rho));
                pointsSphere->push_back(Vec3f(x * radius, y * radius, z * radius));
            }
        }
        pointsSphere->push_back(Vec3f(0.f, 0.f, -radius));
    }
}

// генерация точек конуса - переделано из сферы
void generateConePoints()
{
    pointsCone = new std::vector<Vec3f>;
    // в сфере нижнее сечение представлено одной точкой -> добавим в конце
    int _stacks = stacks - 1;

    float da, r, dr, dz;
    float x, y, z;
    int i, j;
    float topRadius = 0.f , baseRadius = radius, height = 2.f * radius;
    da = (float)M_PI * 2.f/ slices;
    dr = (float)(topRadius - baseRadius) / _stacks;
    dz = (float)height / _stacks;

    if(isSolid)
    {
        float ds =(float) (1.0f / slices);
        float dt =(float) (1.0f / _stacks);
        float t = 0.0f;
        z = -1.0f;
        r = baseRadius;
        //glBegin(GL_TRIANGLE_FAN);
        pointsCone->push_back(Vec3f(0.0f, 0.0f, -1.0f));
        for (i = 0; i <= slices; i++) {
            if (i == slices) {
                x = sin(0.0f);
                y = cos(0.0f);
            } else {
                x = sin((i * da));
                y = cos((i * da));
            }
            pointsCone->push_back(Vec3f((x * r), (y * r), z));

        }
        //glEnd();
        for (j = 0; j < _stacks-1; j++) {
            float s = 0.0f;
            //glBegin(GL_QUAD_STRIP);
            for (i = 0; i <= slices; i++) {
                if (i == slices) {
                    x = sin(0.0f);
                    y = cos(0.0f);
                } else {
                    x = sin((i * da));
                    y = cos((i * da));
                }
                pointsCone->push_back(Vec3f((x * r), (y * r), z));
                pointsCone->push_back(Vec3f((x * (r + dr)), (y * (r + dr)), (z + dz)));
                s += ds;
            } // for slices
            //glEnd();
            r += dr;
            t += dt;
            z += dz;
        } // for _stacks
        //glBegin(GL_TRIANGLE_FAN);
        pointsCone->push_back(Vec3f(0.0f, 0.0f, (radius*2.f - 1.f)));
        for (i = 0; i <= slices; i++) {
            if (i == slices) {
                x = sin(0.0f);
                y = cos(0.0f);
            } else {
                x = sin((i * da));
                y = cos((i * da));
            }
            pointsCone->push_back(Vec3f((x * r), (y * r), z));
        }
        //glEnd();
    }
    else
    {
        pointsCone->push_back(Vec3f(0.0f, 0.0f, -1.0f));
        for (i = 0; i < slices; i++) {
            x = (float)cos((i * da));
            y = (float)sin((i * da));
            z = -1.0f;
            r = baseRadius;
            for (j = 0; j <= _stacks-1; j++) {
                pointsCone->push_back(Vec3f((x * r), (y * r), z));
                z += dz;
                r += dr;
            }
        }
        pointsCone->push_back(Vec3f(0.0f, 0.0f, (radius * 2.f - 1.f)));
    }
}

// переключение режимов и управление морфингом
// важно обнуляется pointsMorph
void keyHandlerMorphing(unsigned char key, int xmouse, int ymouse)
{
    switch (key) {
    // перемещение точечного красного света
    case ',':
        isSolid = true;
        generateSpherePoints();
        generateConePoints();
        delete pointsMorph; // т.к. в Solid больше точек -> будут висячие
        pointsMorph = new std::vector<Vec3f>(pointsSphere->size());
        glutPostRedisplay();
        break;

    case '.':
        isSolid = false;
        generateSpherePoints();
        generateConePoints();
        delete pointsMorph;
        pointsMorph = new std::vector<Vec3f>(pointsSphere->size());
        glutPostRedisplay();
        break;

        // пересчитываем процент морфинга
    case '[':
        morphStage = morphStage >= 1.0f ? 1.0f : (morphStage + 0.01f);
        glutPostRedisplay();
        break;

    case ']':
        morphStage = morphStage <= 0.0f ? 0.0f : (morphStage - 0.01f);
        glutPostRedisplay();
        break;
    }
}

/* EOLab1_Morphing
 * ------------------------------------------------
 */

// управление крутилкой
void Special_Keys (int key, int x, int y)
{
    int window = glutGetWindow();
    switch (window)
    {
    case 1:
        switch (key) {

        case GLUT_KEY_LEFT :  yangle1 -= 2;  break;
        case GLUT_KEY_RIGHT:  yangle1 += 2;  break;
        case GLUT_KEY_UP   :  xangle1 -= 2;  break;
        case GLUT_KEY_DOWN :  xangle1 += 2;  break;
        case GLUT_KEY_PAGE_DOWN: zangle1 -= 2; break;
        case GLUT_KEY_PAGE_UP: zangle1 += 2; break;
        default: return;
        }
        break;
    case 2:
        switch (key) {

        case GLUT_KEY_LEFT :  yangle2 -= 2;  break;
        case GLUT_KEY_RIGHT:  yangle2 += 2;  break;
        case GLUT_KEY_UP   :  xangle2 -= 2;  break;
        case GLUT_KEY_DOWN :  xangle2 += 2;  break;
        case GLUT_KEY_PAGE_DOWN: zangle2 -= 2; break;
        case GLUT_KEY_PAGE_UP: zangle2 += 2; break;
        default:
            return;
        }
        break;
    case 3:
        switch (key) {

        case GLUT_KEY_LEFT :  yangle3 -= 2;  break;
        case GLUT_KEY_RIGHT:  yangle3 += 2;  break;
        case GLUT_KEY_UP   :  xangle3 -= 2;  break;
        case GLUT_KEY_DOWN :  xangle3 += 2;  break;
        case GLUT_KEY_PAGE_DOWN: zangle3 -= 2; break;
        case GLUT_KEY_PAGE_UP: zangle3 += 2; break;
        default:
            return;
        }
        break;
    }



    glutPostRedisplay();
}

// обнавляем выделенную область. остальное нафиг
void reshape ( int width, int height ) {

    /* define the viewport transformation */
    glViewport(0,0,width,height);
}

// текстурные штуки
GLuint loadTexture(Image* image) {
    GLuint textureId; // текстурки характеризуются id
    glGenTextures(1, &textureId); // включаем генерацию текстурных координат
    glBindTexture(GL_TEXTURE_2D, textureId); //Tell OpenGL which texture to edit
    //Map the image to the texture
    glTexImage2D(GL_TEXTURE_2D,           //Always GL_TEXTURE_2D
                 0,                            //0 for now
                 GL_RGB,                       //Format OpenGL uses for image
                 image->width, image->height,  //Width and height
                 0,                            //The border of the image
                 GL_RGB, //GL_RGB, because pixels are stored in RGB format
                 GL_UNSIGNED_BYTE, //GL_UNSIGNED_BYTE, because pixels are stored
                 //as unsigned numbers
                 image->pixels);               //The actual pixel data

    return textureId; //Returns the id of the texture
}

int main ( int argc, char * argv[] ) {

    //initialize GLUT, using any commandline parameters passed to the program
    glutInit(&argc,argv);

    // setup the size, position, and display mode for new windows
    glutInitWindowSize(800, 800);
    glutInitWindowPosition(0,0);
    // говорим, что надо создать буфферы цвета и глубины
    glutInitDisplayMode(GLUT_RGB| GLUT_DEPTH);

    // загружаем текстуру imageloader'ом
    Image* image = loadBMP("pict.bmp");

    // create and set up a window
    glutCreateWindow("Inglourious Primitives");
    {
        // инициализируем квадрик, говорит генерить текст. координаты и быть проволочным
        cone = gluNewQuadric();
        gluQuadricDrawStyle(cone, GLU_LINE);
        gluQuadricTexture(cone, GL_TRUE);
        gluQuadricNormals(cone, GLU_SMOOTH);

        _textureId = loadTexture(image);

        // применяем функции к окошку
        glutReshapeFunc(reshape);
        glutDisplayFunc(display_lab1);
        glutSpecialFunc(Special_Keys);

        // куллфейс позволяет скрывать невидимые грани. по умолчанию скрывает видимые грани
        //    glEnable(GL_CULL_FACE);
        glEnable(GL_DEPTH_TEST); // включаем глубину изображения

        glMatrixMode(GL_PROJECTION); // устанавливаем матрицу проекции
        glLoadIdentity();
        gluPerspective(40,1,1,20); // угол обзора камеры, ???, передняя и задняя границы вида

        glMatrixMode(GL_MODELVIEW); // все следует делать в этой матрице
        glLoadIdentity();

        // позиция камеры
        gluLookAt(0.0,0.0,3.0, // позиция камеры
                  0.0,0.0,0.0, // центр точки обзора
                  0.0,1.0,0.0); // что-то с осями, лучше не трогать
    }

    glutCreateWindow("Inglourious Lights");
    {

        _textureId = loadTexture(image);

        glutDisplayFunc(display_lab2);
        glutReshapeFunc(reshape);
        glutKeyboardFunc(keyHandlerLab2);
        glutSpecialFunc(Special_Keys);

        //glEnable(GL_CULL_FACE);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_ALPHA_TEST); // включаем чувствительность к альфа каналу(прозрачность)
        glEnable(GL_BLEND); // смешение цветов
        glEnable(GL_NORMALIZE); // нормализация нормалей
        glShadeModel(GL_SMOOTH); // мягкие тени. теней нет, so...
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // алгоритм смешения цвевтов
        glDisable(GL_COLOR_MATERIAL); // выключаем окраску color'ом -> будем красить материалами

        // задаем освещение
        createLights();

        // define the projection transformation
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluPerspective(40,1,4,20);

        // define the viewing transformation
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        gluLookAt(5.0,5.0,5.0,
                  0.0,0.0,0.0,
                  0.0,1.0,0.0);
    }

    // удаляем бмп с текстуркой
    delete image;

    glutCreateWindow("Inglourious Morphing");
    {
        glutDisplayFunc(morphing);
        glutReshapeFunc(reshape);
        glutSpecialFunc(Special_Keys);
        glutKeyboardFunc(keyHandlerMorphing);

        glEnable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glEnable(GL_NORMALIZE);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluPerspective(40,1,1,20);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        gluLookAt(3.0,2.0,4.0,
                  0.0,0.0,0.0,
                  0.0,1.0,0.0);

        // изначальная генерация точек фигур. морф рассчитается при первом вызове display
        generateSpherePoints();
        generateConePoints();
        pointsMorph = new std::vector<Vec3f>(pointsSphere->size());

    }

    /* tell GLUT to wait for events */
    glutMainLoop();
}

